import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();

        Scanner sc = new Scanner(System.in);
        List<Integer> values = new LinkedList<>();
        int r;

        //add values to list
        do {
            r = sc.nextInt();
            values.add(r);
        } while (r != 0);

        //loop to do and print finished matrixs(all descriptions are with methods)
        for (int i = 0; i < values.size(); i++) {

            if (values.get(i) > 0) {
                String[][] matrix = new String[2 * values.get(i)][2 * values.get(i)];
                main.basicMatrixWithQuestionMark(matrix);
                main.addStarsInCorners(matrix);
                main.counterClockwiseMatrix(matrix, values.get(i));
                main.changeQuestionMarkToDot(matrix);
                main.printMatrix(matrix);
            } else if (values.get(i) < 0) {
                int rp = values.get(i) * (-1);
                String[][] matrix = new String[2 * rp][2 * rp];
                main.basicMatrixWithQuestionMark(matrix);
                main.addStarsInCorners(matrix);
                main.clockwiseMatrix(matrix, rp);
                main.changeQuestionMarkToDot(matrix);
                main.printMatrix(matrix);
            } else {
                break;
            }
        }

    }

    //add question marks to matrix
    public void basicMatrixWithQuestionMark(String[][] mat) {

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                mat[i][j] = "?";
            }
        }
    }

    //add stars in all corners in matrix
    public void addStarsInCorners(String[][] mat) {
        //from [0][0] to [n][n]
        int i = 0;
        int j = 0;
        do {
            mat[i][j] = "*";
            i++;
            j++;
        } while (i < mat.length && j < mat.length);
        //from [0][n] to [n][0]
        i = 0;
        j = mat.length - 1;
        do {
            mat[i][j] = "*";
            i++;
            j--;
        } while (i < mat.length && j > -1);
    }

    //add stars to clockwise matrix
    public void clockwiseMatrix(String[][] mat, int rp) {
        for (int k = 0; k < rp; k++) {
            //first wing
            for (int j = k; j < rp; j++) {
                int i = 0;
                if (k > 0) {
                    i = k;
                }
                mat[i][j] = "*";
            }
            //second wing
            for (int i = k; i < rp; i++) {
                int j = mat.length - 1;
                if (k > 0) {
                    j -= k;
                }
                mat[i][j] = "*";
            }
            //third wing
            for (int j = (mat.length - 1) - k; j > rp - 1; j--) {
                int i = mat.length - 1;
                if (k > 0) {
                    i -= k;
                }
                mat[i][j] = "*";
            }
            //fourth wing
            for (int i = (mat.length - 1) - k; i > rp - 1; i--) {
                int j = 0;
                if (k > 0) {
                    j += k;
                }
                mat[i][j] = "*";
            }
        }
    }

    //add stars to counterclockwise matrix
    public void counterClockwiseMatrix(String[][] mat, int r) {
        for (int k = 0; k < r; k++) {
            //first wing
            for (int j = (mat.length - 1) - k; j > r - 1; j--) {
                int i = 0;
                if (k > 0) {
                    i = k;
                }
                mat[i][j] = "*";
            }
            //second wing
            for (int i = k; i < r; i++) {
                int j = 0;
                if (k > 0) {
                    j += k;
                }
                mat[i][j] = "*";
            }
            //third wing
            for (int j = k; j < r; j++) {
                int i = mat.length - 1;
                if (k > 0) {
                    i -= k;
                }
                mat[i][j] = "*";
            }
            //fourth wing
            for (int i = (mat.length - 1) - k; i > r - 1; i--) {
                int j = mat.length - 1;
                if (k > 0) {
                    j -= k;
                }
                mat[i][j] = "*";
            }
        }
    }

    //change remaining question marks to dots
    public void changeQuestionMarkToDot(String[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                if (mat[i][j].equals("?")) {
                    mat[i][j] = ".";
                }
            }
        }
    }

    //print done matrix
    public void printMatrix(String[][] mat) {
        for (String[] mark : mat) {
            for (int j = 0; j < mat.length; j++) {
                System.out.print(mark[j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
